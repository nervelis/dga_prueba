//Inicio función general de acceso
$(document).ready(function() {

       //Inicio funcion para validacion de formulario de ingreso
	
	$("#enviar-btn").click(function() {


               //Obtenemos el valor del documento de identidad
		var documento = $("#documento").val();

		//Validamos el campo documento, simplemente miramos que no esté vacío
		if (documento == "") {
			alert("Debe ingresar documento de identificación");
			return false;
		};


		//Obtenemos el valor del campo nombre
		var name = $("#name").val();

		//Validamos el campo nombre, simplemente miramos que no esté vacío
		if (name == "") {
			alert("Debe ingresar nombre");
			return false;
		};

		//Obtenemos el valor del campo apellido
                var apellido = $("#apellido").val();

                //Validamos el campo apellido, simplemente miramos que no esté vacío
                if (apellido == "") {
                        alert("Debe ingresar apellido");
                        return false;
                };

		//Obtenemos el valor del campo username
                var username = $("#username").val();

                //Validamos el campo username, simplemente miramos que no esté vacío
                if (username == "") {
                       alert("Debe ingresar username");
                       return false;
                };

               
		//Obtenemos el valor del campo password
		var password = $("#password").val();

		//Validamos el campo password, simplemente miramos que no esté vacío
		if (password == "") {
			alert("Debe ingresar password");
			return false;
		};

		//Obtenemos el valor del campo password_confirm
                var password_confirm = $("#password_confirm").val();

                //Validamos el campo password_confirm, para que sea igual al campo password
                if (password_confirm !=password) {
                        alert("Confirmación de password es diferente al password");
                        return false;
                };       


               //Obtenemos el valor del campo correo
                var email = $("#email").val();


                // Expresion regular para validar el correo
                var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

                // Se utiliza la funcion test() nativa de JavaScript
                    if (regex.test($('#email').val().trim())) {
                        
                    } else {
                        alert("Formato de email no válido");
                        return false;
                    };


		
		//Construimos la variable que se guardará en el data del Ajax para pasar al archivo php que procesará los datos
		var array = new Object();
                array['documento']=documento;
                array['name']=name;
		array['apellido']=apellido;
                array['username']=username;
		array['password']=password;
		array['password_confirm']=password_confirm;
                array['email']=email;
               
                var x = document.getElementById('registro_form');
		$.ajax({
			data: array,
                        cache: false,
			type: "POST",		
			url: '?controlador=Index&accion=registro',
  		        success: function(data) {
                       	var res=jQuery.parseJSON(data);
                        
                        if(res.error == 1){             
                           alert(res.error_msg);
		       };
                       if(res.error==2){
                          alert(res.error_msg);
                          x.style.display = 'none';
			};  
                       }
                   
			
		});//Fin de ajax

                return false;

          
        });

       //Fin funcion para validacion de formulario de ingreso

       //Inicio funcion para validacion validar ingreso
       $("#entrar").click(function() {

		//Obtenemos el valor del campo correo
                var usuario = $("#usuario").val();


                // Expresion regular para validar el correo
                var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

                // Se utiliza la funcion test() nativa de JavaScript
                    if (regex.test($('#usuario').val().trim())) {
                        
                    } else {
                        alert("Debe ingresar su usuario/email con un formato válido");
                        return false;
                    };

		//Obtenemos el valor del campo apellido
                var contrasena = $("#contrasena").val();

                //Validamos el campo apellido, simplemente miramos que no esté vacío
                if (contrasena == "") {
                        alert("Debe ingresar su contraseña");
                        return false;
                };
                 
		//Construimos la variable que se guardará en el data del Ajax para pasar al archivo php que procesará los datos
		var array = new Object();
                array['usuario']=usuario;
		array['contrasena']=contrasena;
		
                var x = document.getElementById('ingresa_form');
		$.ajax({
			data: array,
                        cache: false,
			type: "POST",		
			url: '?controlador=Index&accion=ingreso',
  		        success: function(data) {
                       	var res=jQuery.parseJSON(data);
                     
                      if(res.error==1){   
                          var usuarioresp=res.usuario;
                          var contrasenaresp=res.contrasena;	 
                          submitUsuarioRegistrado(usuarioresp,contrasenaresp);
			}

                       if(res.error==2){                             	
                          alert(res.error_msg);
                          x.style.display = 'none';
			}

                       if(res.error==3){                             	
                          alert(res.error_msg);
                          x.style.display = 'none';
			}

                         

 				                         
		  }	
		});//Fin de ajax

                return false;
       });//Fin funcion para validacion validar ingreso
}); //Fin función general de acceso

		function registroFunction() {
			var x = document.getElementById('registro_form');
			var y = document.getElementById('ingresa_form');
			var z = document.getElementById('ingresa');
			if (x.style.display === 'none') {
				x.style.display = 'block';
                                y.style.display = 'none';
				z.style.display = '3px solid rgb(255,255,255)';
			} else {
				x.style.display = 'none';
			}
			
		}

		function ingresaFunction() {
			var x = document.getElementById('ingresa_form');
			var y = document.getElementById('registro_form');
			var z = document.getElementById('ingresa');
			if (x.style.display === 'none') {
				y.style.display = 'none';
				x.style.display = 'block';
				z.style.border = '3px solid rgb(205,173,28)';
			} else {
				x.style.display = 'none';
				z.style.border = '3px solid rgb(255,255,255)';
			}
			
		}



               function submitUsuarioRegistrado(usuario,contrasena) {
                        var usuario=usuario;
                        var contrasena=contrasena;
                        var form = document.ingresar;
                        form.variable1.value=usuario;
                        form.variable2.value=contrasena;
 			form.action="?controlador=Index&accion=goPerfilUsuarioregistrado";
			form.submit();
		}

