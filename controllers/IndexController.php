<?php
class IndexController
{
    function __construct()
    {
        //Creamos una instancia de nuestro mini motor de plantillas
        $this->view = new View();
    }

    //Accion index
    public function index()
    {      
		$data=1;
		$this->view->show("index.php",$data);
    }
	

	public function registro()
	{
	  	session_start(); // inicio sesiones.
                //include 'config/configemail.php';

	        require 'models/UsuarioRegistradoModel.php';
		//Creamos una instancia de nuestro "modelo"
		$usuarioRegister = new UsuarioRegistradoModel();
                $response=array(
			  "success"=>0,
		          "error"=>0,
                          "error_msg"=>""
		);
                $documento = $_POST['documento'];
                $name = $_POST['name']; 
                $apellido = $_POST["apellido"];
                $username = $_POST["username"];
                $password = $_POST["password"];
                $password_confirm = $_POST["password_confirm"];
                $email = $_POST["email"];
                
                $cant_usuario=0;
    		$cod_act = substr(md5(rand()),0,16); 
                $passwordseguro=md5($password);
                
                $verificacionemail=$usuarioRegister->buscarUsuario($email,$username);
	        while($verificacion = $verificacionemail->fetch())
			{
				$cantidad_usuario=$verificacion['cuenta'];
			}
                // Verificación de que usuario no este registrado
		
                if($cantidad_usuario>0){
			$response["error"] = 1; //Guardamos el código de respuesta
			$response["error_msg"] = "Usuario ya se encuentra registrado, verifique username o email ingresado"; //Guardamos el mensaje de error
			
		}else{
                        $insertar=$usuarioRegister->insertarUsuario($documento,$username,$passwordseguro,$email,$cod_act,$name,$apellido);
                        
                        
			//$to=$email;
                        //$from= "http://www.sldga.cl";
                        //$base_url='http://www.sldga.cl/views/';
			//$subject="Solicitud para activar su cuenta en el portal sldga.cl";
			//$body='Saludos, <br/> <br/>Bienvenido(a), Por favor necesitamos que verifique su correo electrónico para activar su cuenta en nuestro sitio  web. <br/> <br/> <a href="'.$base_url.'activation.php?cod_act='.$cod_act.'">'.$base_url.'activation.php?cod_act='.$cod_act.'</a>';

                       
			 
			//Agregar destinatario
                        //$mail->SetFrom($from, 'From Name');

			//$mail->AddAddress($to);
			//$mail->Subject = $subject;
			//$mail->Body = $body;
			//Para adjuntar archivo
			
			//$mail->MsgHTML($body);
			 
			//Avisar si fue enviado o no y dirigir al index
			//$mail->Send();
			
			$response["error"] = 2; //Guardamos el código de respuesta
			$response["error_msg"] = "Usuario registrado satisfactoriamente";  //Guardamos el mensaje de error
					
	       }
            
                       
                        echo json_encode($response); //Devolvemos la respuesta en JSON	
}




     public function ingreso()
     {
	  	session_start(); // inicio sesiones.
	        require 'models/UsuarioRegistradoModel.php';
		//Creamos una instancia de nuestro "modelo"
		$usuarioRegister = new UsuarioRegistradoModel();
                $response=array(
			  "success"=>0,
		          "error"=>0,
                          "error_msg"=>""
		);

                  
                $usuario = $_POST["usuario"]; 
                $contrasena = trim($_POST["contrasena"]);               
                $compararpass=md5($contrasena); 
                $verificacionemail=$usuarioRegister->validarUsuario($usuario);
	        $resultado=$verificacionemail->rowCount();
              if($resultado>0){
	              while($verificacion = $verificacionemail->fetch())
			{
				$_SESSION['usuarioid']=$verificacion['id'];
				$_SESSION['status']=$verificacion['status'];
				$_SESSION['username']=$verificacion['username'];
                                $_SESSION['password']=trim($verificacion['password']);
                                $_SESSION['email']=$verificacion['email'];
			        
			}
                
     
              
                    if(($_SESSION['status']==1)&&($_SESSION['password']==$compararpass)){
			$response["error"] = 1; //Guardamos el código de respuesta
                        $response["error_msg"] = "Usuario valido";
			$response["usuario"] = $usuario;
                        $response["contrasena"] = $contrasena;
				
		   }else{
                       $response["error"] = 2; //Guardamos el código de respuesta
			$response["error_msg"] = "password incorrecto";
			

                   }
  
	    }else{
                        $response["error"] = 3; //Guardamos el código de respuesta
			$response["error_msg"] = "Usuario no se encuentra registrado";
		        
			
             }
             echo json_encode($response); //Devolvemos la respuesta en JSON  

     }

    

      
     
     

      public function cerrarSesion()
	{
          // Inicializar la sesión.
	 // Si está usando session_name("algo"), ¡no lo olvide ahora!		
	session_start();

	// Destruir todas las variables de sesión.
	$_SESSION = array();

	// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
	// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
	if (ini_get("session.use_cookies")) {
	    $params = session_get_cookie_params();
	    setcookie(session_name(), '', time() - 42000,
		$params["path"], $params["domain"],
		$params["secure"], $params["httponly"]
	    );

         $this->view->show("home.php");    
	}
	
	// Finalmente, destruir la sesión.
		session_destroy();
	}
 

     public function goRecuperarclave()
	{
		session_start(); // inicio sesiones.
                include 'config/configemail.php';
                require 'models/UsuarioRegistradoModel.php';
		//Creamos una instancia de nuestro "modelo"
		$usuarioRegister = new UsuarioRegistradoModel();
               
                $_SESSION["mensaje"] =  $_POST["mensaje2"];
                $email = $_POST["correo"];
                
		
		$usuarioregistrado=$usuarioRegister->buscarPerfilUsuarioregistrado($email);
		$usuarioarreglo=Array();
               
		
		while($datausuario = $usuarioregistrado->fetch())
		{
			$usuarioarreglo[1]['id']=$datausuario['id'];
			$usuarioarreglo[1]['username']=$datausuario['username'];
			$usuarioarreglo[1]['password']=$datausuario['password'];
	                $usuarioarreglo[1]['email']=$datausuario['email'];
		}

			$to=$email;
                        $from= "http://www.sibeliusfest.com/ve";
                        $base_url='http://sibelius.nervelis.com.ve/views/';
			$subject="Recuperación de datos para acceder al Portal Sibelius Fest";
			$subject="=?ISO-8859-1?B?".base64_encode($subject)."=?=";
			$body='Saludos, <br/>sus datos para acceder son <br/>Usuario:'.$usuarioarreglo[1]['username'].'<br/> Password:'.$usuarioarreglo[1]['password'];

                   
			//Agregar destinatario
                        $mail->SetFrom($from, 'From Name');

			$mail->AddAddress($to);
			$mail->Subject =  $subject;
			$mail->Body =  $body;
			//Para adjuntar archivo
			
			$mail->MsgHTML($body);
			 
			//Avisar si fue enviado o no y dirigir al index
			$mail->Send();
                            
		//Finalmente presentamos nuestra plantilla
		$this->view->show("perfil_notificaciones.php");
	}




  public function goPerfilUsuarioregistrado()
	{
	session_start(); // inicio sesiones.
	$arrEstados = Array();
        $arrGenero = Array();
        if(!isset($_SESSION["usuarioregistrado"])){
		$usuario =  $_POST["variable1"];
		$contrasena = $_POST["variable2"]; 
		
        }
      
  
	if(!isset($_SESSION["usuarioregistrado"])){
		//Incluye el modelo que corresponde
		require 'models/UsuarioRegistradoModel.php';
		
		      
		//Creamos una instancia de nuestro "modelo"
		$consultaregistroUsuario = new UsuarioRegistradoModel();		
		$usuarioregistrado=$consultaregistroUsuario->buscarPerfilUsuarioregistrado($usuario);
		$usuarioarreglo=Array();
                $usuarioarreglo2=Array();
                $usuarioarreglo3=Array();
               
               
		
		while($datausuario = $usuarioregistrado->fetch())
		{

			$usuarioarreglo[1]['username']=$datausuario['username'];
			$usuarioarreglo[1]['password']=$datausuario['password'];
	                $usuarioarreglo[1]['email']=$datausuario['email'];
			$usuarioarreglo[1]['status']=$datausuario['status'];
			$usuarioarreglo[1]['nombre']=$datausuario['nombre'];
			$usuarioarreglo[1]['apellido']=$datausuario['apellido'];
				
		}

		
		$_SESSION["usuarioregistrado"]=$usuarioarreglo;
                
        } // fin de verificacion de sesion usuario
	
          
		
		//Finalmente presentamos nuestra plantilla
		$this->view->show("perfil_usuarioregistrado.php");
	}



    

}
?>
