<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/pruebadga.js"></script>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>DGA - Prueba</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="css/landing-page.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-light bg-light static-top">

    <div class="container-fluid" style="width: 100%;">
      <div style="width: 10%;">
         <a class="navbar-brand" href="http://www.sldga.cl/" target="_blank"><img class="img-thumbnail" src="img/dga_logo.jpg" alt=""></a>
      </div>
     <div style="width: 40%;">
         &nbsp;
     </div>
    
    <div style="width: 15%;">
      <a href="#" class="text-success"><h6>Nuestra empresa</h6></a>
    </div>
    <div style="width: 10%;">
      <a href="#" class="text-success"><h6>Servicios</h6></a>
    </div>
    <div style="width: 10%;">
      <a href="#" class="text-success"><h6>Contacto</h6></a>
    </div>
    
   <div id="registro" style="width: 10%;">
      <a class="btn btn-success" href="javascript:registroFunction();">Registrarse</a>
    </div>
    <div id="ingresa" style="width: 5%;">
      <a class="btn btn-success" href="javascript:ingresaFunction();">Ingresar</a>
    </div>
  </div>



<!-- Registro y/o Autenticacion -->   
                  <div id="registro_form" style="position:absolute; z-index:2; right:2%; top:100px; opacity:0.9;display:none">
                        <form name="register" method="post" action="" id="register">
                                                <input type="text" name="documento" id="documento" placeholder="Documento identificación" maxlength="38"><br>
                                                <input type="text" name="name" id="name" placeholder="Nombre" maxlength="38"><br>
                                                <input type="text" name="apellido" id="apellido" placeholder="Apellido" maxlength="38"><br>
                                                <input type="text" name="username" id="username" placeholder="Username" maxlength="38"><br>
                                                <input type="password" name="password" id="password" placeholder="Contraseña" maxlength="38"><br>
                                                <input type="password" name="password_confirm" id="password_confirm" placeholder="Confirmación contraseña" maxlength="38"><br>
                                                 <input type="text" name="email" id="email" placeholder="Correo electrónico" maxlength="38"><br>
                                                                                              
 						<input name="submit" type="submit" value="Enviar" id="enviar-btn"/>
						
                        </form>                
                  </div>

                    <div id="ingresa_form" style="position:absolute; z-index:2; right:1%; top:100px; opacity:0.9;display:none">
			  <form name="ingresar" method="post" action="" id="ingresar">  
						<input type="text" name="usuario" id="usuario" placeholder="Correo electrónico"><br>
						<input type="password" name="contrasena" id="contrasena" placeholder="Contraseña"><br>

						<div class="box">
							  <input type="checkbox" id="c1" name="cb">Recordar cuenta
						
						</div>
						<input type="submit"  name="entrar" id="entrar" value="Ingresar"><br><br>
					             <a>¿Olvidaste tu contraseña?</a>
					        <input type="hidden" name="variable1" id="variable1" value="">
						<input type="hidden" name="variable2" id="variable2" value="">
			</form>
		  </div>
  </nav>
  

  <!-- Masthead -->
  <header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto">
          <h1 class="mb-5">SERVICIOS LOGÍSTICOS DGA S.A.</h1>
        </div>
        <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <form>
            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0">
                <input type="email" class="form-control form-control-lg" placeholder="Enter your email...">
              </div>
              <div class="col-12 col-md-3">
                <button type="submit" class="btn btn-block btn-lg btn-success">Registrarse!</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </header>



  
 
  <!-- Testimonials -->
  <section class="testimonials text-center bg-light">
    <div class="container">
      <h2 class="mb-5">Clientes</h2>
      <div class="row">
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="img/falabella_test.png" alt="">
            <h5>Falabella</h5>
            <a href="https://www.falabella.com" class="text-secondary"><p class="font-weight-light mb-0">"Falabella es una de las compañías más grandes y consolidadas de América Latina. Desarrolla su actividad comercial a través de varias áreas de negocio, siendo las principales, la tienda por departamentos, grandes superficies, mejoramiento y construcción del hogar, compañía de financiamiento comercial CMR, banco, viajes y seguros Falabella."</p></a>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="img/falabella_test.png" alt="">
            <h5>Falabella</h5>
            <a href="https://www.falabella.com" class="text-secondary"><p class="font-weight-light mb-0">"Falabella es una de las compañías más grandes y consolidadas de América Latina. Desarrolla su actividad comercial a través de varias áreas de negocio, siendo las principales, la tienda por departamentos, grandes superficies, mejoramiento y construcción del hogar, compañía de financiamiento comercial CMR, banco, viajes y seguros Falabella."</p></a>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="img/falabella_test.png" alt="">
            <h5>Falabella</h5>
            <a href="https://www.falabella.com" class="text-secondary"><p class="font-weight-light mb-0">"Falabella es una de las compañías más grandes y consolidadas de América Latina. Desarrolla su actividad comercial a través de varias áreas de negocio, siendo las principales, la tienda por departamentos, grandes superficies, mejoramiento y construcción del hogar, compañía de financiamiento comercial CMR, banco, viajes y seguros Falabella."</p></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  
  <!-- Footer -->
  <footer class="footer bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <ul class="list-inline mb-2">
            <li class="list-inline-item">
              <a href="#" class="text-white">Nuestra empresa</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#" class="text-white">Servicios</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="#" class="text-white">Contacto</a>
            </li>
            
          </ul>
          <a href="https://www.linkedin.com/in/nervelis-diaz-atencio-66a53621/" class="text-white"><p class="text-muted small mb-4 mb-lg-0">&copy;2019 Nervelis Díaz/Software Engineer.</p></a>
        </div>
        <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-3">
              <a href="#" class="text-white">
                <i class="fab fa-facebook fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item mr-3">
              <a href="#" class="text-white">
                <i class="fab fa-twitter-square fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#" class="text-white">
                <i class="fab fa-instagram fa-2x fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
